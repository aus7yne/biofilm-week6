<?php
class Contact
{
    public static function send($param)
    {
        date_default_timezone_set("Africa/Lagos");

        //open or create contact.csv
        $file = fopen('contact.csv', 'a');

        //create column
        fputcsv($file, array('Name', 'Email', 'phone', 'Comment', 'Date'));
        fputcsv($file, array($param['name'], $param['email'], $param['phone'], $param['comment'], date("Y-m-d")));
        fclose($file);

        $message = "Hello " . $param['name'] . ",\n\nThank you for contacting us.\n\nRegards.";
        $headers = "Reply-To: BioFilms Admin <biofilms@rentals.com\r\n";
        $headers .= "Return-Path: BioFilms Admin <biofilms@rentals.com\r\n";
        $headers = "From: BioFilms Admin <biofilms@rentals.com\r\n";
        $subject = "Booust Contact";
        //send mail
        mail($param['email'], $subject, $message, $headers);

        echo ("<script LANGUAGE='JavaScript'> alert('Thank you for contacting us!!! An email has been forwarded to you. Please check your mail.');</script>");

        echo '<h3> Reading the csv file: </h3>'; 
        $csv = array_map('str_getcsv', file('contact.csv'));
        echo $csv[sizeof($csv) - 1][0] . '<br>';
        echo $csv[sizeof($csv) - 1][1] . '<br>';
        echo $csv[sizeof($csv) - 1][2] . '<br>';
        echo $csv[sizeof($csv) - 1][3] . '<br>';
        echo $csv[sizeof($csv) - 1][4] . '<br>';

    }
}
