<?php
ob_start();
require __DIR__ . '/include/login-tracker.php';

if($user!=null):
    header("Location: default.php");
else:

$login_error_message = '';
 
// check Register request
if (!empty($_POST['send'])) {
    require __DIR__ . '/include/config.php';
    require __DIR__ . '/include/lib.php';
    $object = new RegisterLogin();

    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    if ($email == "") {
        $login_error_message = 'email field is required!';
    } else if ($password == "") {
        $login_error_message = 'Password field is required!';
    } else {
        $id = $object->Login($email, $password); // check user login
        if ($id > 0) {
            $_SESSION['id'] = $id; // Set Session
            header("Location: default.php");
            
        } else {
            $login_error_message = 'Invalid login details!';
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script>

    <link rel="icon" href="./img/logo.png">

    <title>BioFilm Rental</title>
</head>

<body>
    <div class="container-fluid" style="background: /* top, transparent red */
  linear-gradient(to right, rgb(139, 192, 241), rgba(255, 255, 255, 0.99)), /* bottom, image */
  url('./img/landscape_movies_looper_poster_1.jpg');">
        <header>
            <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-primary">
                <a class="navbar-brand" href="#">BioFilms Rental Company</a>
                <!--Add here -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!--Add here -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="default.php"> Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="default.php#service">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="default.php#about">About us</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="movie.php" id="navbarDropdown">
                                Movies
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="contact.php">Contact us</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" href="sign-in.php">Login</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="sign-up.php">Register</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="container-fluid" style="padding-top: 57px">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="default.html">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="Conta">Contact</li>
                </ol>
            </nav>

            <div class="container animated fadeIn">

                <div class="row">
                    <h1 class="header-title"> Sign In </h1>
                    <hr>
                    <div class="col-md-12" id="parent">
                    <?php
                    if ($login_error_message != "") {
                        echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
                    }
                    ?>

                        <div class="col-md-6">
                            <form action="sign-in.php" class="contact-form" method="post">
                                <div class="form-group form_left">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" autocomplete="off" required>
                                </div>
                                
                                <div class="form-group">
                                    <input class="btn btn-lg btn-outline-primary" type="submit" name="send" value="Sign In">
                                </div>
                            </form>
                            <p>Not yet a member, please
                                    <a href="sign-up.html">sign Up</a>
                                </p>
                        </div>
                        
                    </div>
                </div>

                <div class="container second-portion">
                    <div class="row">
                        <!-- Boxes de Acoes -->
                        <div class="col-xs-12 col-md-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                    <div class="info">
                                        <h3 class="title">MAIL & WEBSITE</h3>
                                        <p>
                                            <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp austinemma@outlook.com
                                            <br>
                                            <br>
                                            <i class="fa fa-globe" aria-hidden="true"></i> &nbsp www.gitlab.com
                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                    </div>
                                    <div class="info">
                                        <h3 class="title">CONTACT</h3>
                                        <p>
                                            <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+234)-7064623170
                                            <br>
                                            <br>
                                            <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+234)-7064623170
                                        </p>
                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </div>
                                    <div class="info">
                                        <h3 class="title">ADDRESS</h3>
                                        <p>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp No. 50, Awka, Anambra State, Nigeria.
                                        </p>
                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- FOOTER -->
    <footer class="pt-1 my-md-4 pt-md-2 border-top">
        <div class="row">
            <!-- <div class="col-12 col-md">
        </div> -->
            <div class="col-md-4">
                <a href="default.html">
                    <img style="padding-left: 50px" src="img/logo.png" height="100px" width="80%" alt="logo">
                </a>
            </div>
            <div class="col-md-4">
                <h5>Resources</h5>
                <ul class="list-unstyled text-small">
                    <li>
                        <a class="text-muted" href="#">Resource</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Resource name</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Another resource</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Final resource</a>
                    </li>
                </ul>
            </div>
            <div class="col-6 col-md-4">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li>
                        <a class="text-muted" href="#">Team</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Locations</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Privacy</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Terms</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <script src="js/my-jquery.js">
    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
</body>

</html>
<?php endif; ?>