<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>Leap Years</title>
    <style>
        .dd{
            padding: 1;
            margin: 0;
            text-align: center;
            width: 100%;
            
        }
        .dd1{
            padding: 1;
            margin: 0;
            text-align: center;
            width: 100%;
        }
        .link{
            padding-top: 25%;
            margin: 0;
            text-align: center;
        }
        .link1{
            padding-top: 85%;
            margin: 0;
            text-align: center;
        }
        </style>
</head>
<body><br>
    <center><h1>LEAP YEAR PROGRAM</h1></center><br>
    <div class="row">
        <div class="col-md-3 link">
            <a href="default.php">
                <button class="btn btn-primary">Click to access BioFilms</button>
            </a>
        </div>
        <div class="col-md-6">
<?php

/**
 * Parses and verifies the doc comments for files.
 *
 * PHP version 7
 *
 * @category  PHP
 * @package   BioFilm
 * @author    Austin <ae@gmail.com>
 * @copyright 2006-2014 Squiz Pty Ltd (ABN 77 084 670 600)
 * @license   https://github.com/aus7yne/master/licence.txt BSD Licence
 * @link      http://pear.php.net/package/PHP_CodeSniffer
 */

/* require_once "default.html";*/

$from_year = 1980;
$counter = 0;
$leap_year=0;
foreach (range(date('Y'), $from_year) as $year) {
    if (0 == ($year % 4) && 0 != ($year % 100) || 0 == ($year % 400)) {
        $leap_year = $year;
    }
    if ($year == $leap_year) {
        $counter++;
        print "<div class='alert alert-success dd'>$leap_year - Leap Year <br></div>";
    } else {
        print "<div class='alert alert-danger dd1'>$year <br></div>";
    }
}
?>
    </div>
    <div class="col-md-3 link1">
        <a href="default.php">
            <button class="btn btn-primary">Click to access BioFilms</button>
        </a>
    </div>
        <h1 class="alert alert-success" style="text-align:center;width:100%">
            <?php echo 'We have ' . $counter . ' Leap Years in Total'; ?>
        </h1>
</div>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>
