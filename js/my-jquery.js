$(document).ready(function () {

      $('#output').hide();

      $('#myForm').submit(function (e) {
        e.preventDefault();
        var name = $('#name').val();
        var phone = $('#phone').val();
        var email = $('#email').val();

        $(".error").remove();

        if (name.length < 1) {
          $('#name').after('<span class="error">This field is required</span>');
        }
        if (phone.length < 1) {
          $('#phone').after('<span class="error">This field is required</span>');
        }
        if (email.length < 1) {
          $('#email').after('<span class="error">This field is required</span>');
        } else {          
        $('#loader').addClass('loader').fadeIn(1000);
        $('#output').text('Form Submitted').fadeIn(3000);
        $('#loader').removeClass('loader').fadeOut(4000);

          // var regEx = /^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/;
          // var validEmail = regEx.test(email);
          // if (!validEmail) {
          //   $('#email').after('<span class="error">Enter a valid email</span>');
          // } 
        }
      });

      // Focus & Blur Events	
      $('#myForm input').focus(function () {
        $(this).css('background', 'lightgray');

        $(this).blur(function () {
          $(this).css('background', 'white');
        });

      });

      // Focus & Blur Events	on textarea
      $('#myForm textarea').focus(function () {
        $(this).css('background', 'lightgray');

        $(this).blur(function () {
          $(this).css('background', 'white');
        });
      });

      // Select Event
      $(':input').select(function () {
        $('#output').text('Something was selected').show();
      });

      // Keyup and Keydown Events
      $('#myForm input').keypress(function () {
        $('#output').text('You are typing..').show();
        $(this).keydown(function () {
          $('#output').text('').hide();
        });
      });

    });