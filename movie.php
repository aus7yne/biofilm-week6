<?php
require __DIR__ . '/include/login-tracker.php'
 
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script>

  <link rel="icon" href="./img/logo.png">
 
  <title>BioFilm Rental</title>
</head>

<body>
  <div class="container-fluid" style="background: /* top, transparent red */
  linear-gradient(to right, rgb(139, 192, 241), rgba(255, 255, 255, 0.99)), /* bottom, image */
  url('./img/landscape_movies_looper_poster_1.jpg');">
    <header>
      <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-primary">
        <a class="navbar-brand" href="#">BioFilms Rental Company</a>
    <h4 style="text-align:center;padding-left:15%;color:#00bfff">Welcome: <em><?php echo $user->name; ?></em></h4>
        <!--Add here -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!--Add here -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="default.php"> Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="default.php#service">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="default.php#about">About us</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link active" href="movie.php" id="navbarDropdown">
                Movies
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="contact.php">Contact us</a>
            </li>
            <?php
            if ($user != null) :
                echo '<li class="nav-item"> <a class="nav-link" href="profile.php">Profile</a> </li>';
            echo '<li class="nav-item"> <a class="nav-link" href="logout.php">Logout</a> </li>';
            ?>
            <?php
            else :
            ?>
            <li class="nav-item">
              <a class="nav-link" href="sign-in.php">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="sign-up.php">Register</a>
            </li>
            <?php
            endif;
            ?>
          </ul>
        </div>
      </nav>
    </header>
    <div class="container-fluid" style="padding-top: 57px">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="default.html">Home</a>
          </li>
          <li class="breadcrumb-item active" aria-current="Conta">Movies</li>
        </ol>
      </nav>

      <div class="container-fluid animated fadeIn">

        <div class="container-fluid" style="background-image:url('img/9.jpg');padding-top: 10px " id="movies">
            <h2 style="color:paleturquoise">Action Movies</h2>
            <div class="row">
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/10.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/AVENGER.png" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top" src="img/7.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      
            <h2 style="color:paleturquoise">Love Movies</h2>
            <div class="row">
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/s1.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/s1.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      
            <h2 style="color:paleturquoise">Nigerian Movies</h2>
            <div class="row">
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/N1.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top img-fluid" src="img/N2.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top" src="img/N4.jpg" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                      a little bit longer.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                      </div>
                      <small class="text-muted">9 mins ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>
</div>

    </div>
    <!-- FOOTER -->
    <footer class="pt-1 my-md-4 pt-md-2 border-top">
      <div class="row">
        <!-- <div class="col-12 col-md">
        </div> -->
        <div class="col-md-4">
         <a href="default.html"><img style="padding-left: 50px" src="img/logo.png" height="100px" width="80%" alt="logo"></a>
        </div>
        <div class="col-md-4">
          <h5>Resources</h5>
          <ul class="list-unstyled text-small">
            <li>
              <a class="text-muted" href="#">Resource</a>
            </li>
            <li>
              <a class="text-muted" href="#">Resource name</a>
            </li>
            <li>
              <a class="text-muted" href="#">Another resource</a>
            </li>
            <li>
              <a class="text-muted" href="#">Final resource</a>
            </li>
          </ul>
        </div>
        <div class="col-6 col-md-4">
          <h5>About</h5>
          <ul class="list-unstyled text-small">
            <li>
              <a class="text-muted" href="#">Team</a>
            </li>
            <li>
              <a class="text-muted" href="#">Locations</a>
            </li>
            <li>
              <a class="text-muted" href="#">Privacy</a>
            </li>
            <li>
              <a class="text-muted" href="#">Terms</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
      crossorigin="anonymous"></script>
</body>

</html>