<?php
ob_clean();
require __DIR__ . '/include/login-tracker.php';
$user;
 
?>
<!doctype html>
<html lang="en">

<head> 
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script>

  <link rel="icon" href="./img/logo.png">
 
  <title>BioFilm Rental</title>
</head>

<body>
  <div class="container-fluid" style="background: /* top, transparent red */
  linear-gradient(to right, rgb(139, 192, 241), rgba(255, 255, 255, 0.99)), /* bottom, image */
  url('./img/landscape_movies_looper_poster_1.jpg');">
    <header>
      <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-primary">
        <a class="navbar-brand" href="#">BioFilms Rental Company</a>
    <h4 style="text-align:center;padding-left:15%;color:#00bfff"> <?php if($user!=null):echo 'Welcome:';endif;?> <em><?php if($user!=null):echo $user->name;endif; ?></em></h4>
        <!--Add here -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!--Add here -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link active" href="default.php"> Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="default.php#service">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="default.php#about">About us</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="movie.php" id="navbarDropdown">
                Movies
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="contact.php">Contact us</a>
            </li>
            <?php
            if ($user != null) :
                echo '<li class="nav-item"> <a class="nav-link" href="profile.php">Profile</a> </li>';
            echo '<li class="nav-item"> <a class="nav-link" href="logout.php">Logout</a> </li>';
            ?>
            <?php
            else :
            ?>
            <li class="nav-item">
              <a class="nav-link" href="sign-in.php">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="sign-up.php">Register</a>
            </li>
            <?php
            endif;
            ?>
          </ul>
        </div>
      </nav>
      <br>
      <br>
      <!-- carousel -->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/9.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
              <button class=" btn btn-lg btn-danger ">WATCH NOW</button>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/AVENGER.png" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
              <button class=" btn btn-lg btn-danger ">WATCH NOW</button>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/N5.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
              <button class=" btn btn-lg btn-danger ">WATCH NOW</button>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div></header>

    <br>
    <center>
      <h2 id="service">OUR SERVICES</h2>
      <br>
    </center>
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div class="card mb-4">
          <div class="card-body text-center">
            <img class="rounded-circle" src="img/iconsLineSet3-03-512.png" alt="Generic placeholder image" width="140" height="140">
            <h2 class="card-title">We Sell Movies</h2>
            <p class="card-text">THE BIGGEST NOLLYWOOD MOVIES FIRST! BioFilms Rentals When it comes to the world of entertainment, we are NO.1</p>
            <!-- <a href="#" class="card-link">Another link</a> -->
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <div class="card mb-4">
          <div class="card-body text-center">
            <img class="rounded-circle" src="img/Icon_37-512.png" alt="Generic placeholder image" width="140" height="140">
            <h2 class="card-title">Cinema Watch</h2>
            <p class="card-text">THE BIGGEST NOLLYWOOD MOVIES FIRST! BioFilms Rentals When it comes to the world of entertainment, we are NO.1</p>
            <!-- <a href="#" class="card-link">Another link</a> -->
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <div class="card mb-4">
          <div class="card-body text-center">
            <img class="rounded-circle" src="img/iconsLineSet3-03-512.png" alt="Generic placeholder image" width="140" height="140">
            <h2 class="card-title">We Rent Movies</h2>
            <p class="card-text">THE BIGGEST NOLLYWOOD MOVIES FIRST! BioFilms Rentals When it comes to the world of entertainment, we are NO.1</p>
            <!-- <a href="#" class="card-link">Another link</a> -->
          </div>
        </div>
      </div>
    </div>

    <div id="about" class="jumbotron" style="width: 100%;height: 500px;background-image: url('img/7.jpg');color: white;opacity: 0.9;">

      <h1 class="display-4">Simple. Elegant. Awesome.</h1>
      <p class="lead">THE BIGGEST NOLLYWOOD MOVIES FIRST! BioFilms
        <br>Rentals When it comes to the world of entertainment, we are always the BEST.
      </p>

      <p class="lead">
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
      </p>
    </div>

    <div class="container-fluid" style="background-image:url('img/9.jpg');padding-top: 10px " id="movies">
      <h2 style="color:paleturquoise">Action Movies</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/10.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/AVENGER.png" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="img/7.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <h2 style="color:paleturquoise">Love Movies</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/s1.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/s1.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="img/s1.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <h2 style="color:paleturquoise">Nigerian Movies</h2>
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/N1.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top img-fluid" src="img/N2.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="img/N4.jpg" alt="Card image cap">
            <div class="card-body">
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is
                a little bit longer.</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Watch</button>
                </div>
                <small class="text-muted">9 mins ago</small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- FOOTER -->
  <footer class="pt-1 my-md-4 pt-md-2 border-top">
    <div class="row">
      <!-- <div class="col-12 col-md">
        </div> -->
      <div class="col-md-4">
        <a href="default.html">
          <img style="padding-left: 50px" src="img/logo.png" height="100px" width="80%" alt="logo">
        </a>
      </div>
      <div class="col-md-4">
        <h5>Resources</h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Resource</a>
          </li>
          <li>
            <a class="text-muted" href="#">Resource name</a>
          </li>
          <li>
            <a class="text-muted" href="#">Another resource</a>
          </li>
          <li>
            <a class="text-muted" href="#">Final resource</a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md-4">
        <h5>About</h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Team</a>
          </li>
          <li>
            <a class="text-muted" href="#">Locations</a>
          </li>
          <li>
            <a class="text-muted" href="#">Privacy</a>
          </li>
          <li>
            <a class="text-muted" href="#">Terms</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>
</body>

</html>