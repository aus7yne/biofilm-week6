<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

class Contact
{
    public static function send($param)
    {

            //OPEN OR CREATE contact.csv IF IT DOES NOT EXIST
        $file = fopen("contact.csv", "a");

            //CREATE CSV COLUMNS (RUNS THIS ONCE)
        //fputcsv($file, array('Name', 'Email', 'phone', 'Comment', 'Date'));
        
            // APPEND CONTACT DETAILS TO CSV FILE
        fputcsv($file, array($param['name'], $param['email'], $param['phone'], $param['comment'], date("Y-m-d")));
            
            
        // CLOSE FILE STREAM
        fclose($file);

            //PREPARE EMAIL MESSAGE AND SEND TO USER 
        $message = "Hello " . $param['name'] . ",\n\nThank you for contacting us.\n\nRegards.";
        $headers = "Reply-To: BioFilms Admin <biofilms@rentals.com\r\n";
        $headers .= "Return-Path: BioFilms Admin <biofilms@rentals.com\r\n";
        $headers = "From: BioFilms Admin <biofilms@rentals.com\r\n";
        $subject = "Booust Contact";
            
            //SEND MAIL
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
                //Server settings
            $mail->SMTPDebug = 4;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->SMTPAuth = false;                               // Enable SMTP authentication
            $mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->Port = 587;                                    // TCP port to connect to
            $mail->Username = 'etikadb@gmail.com';                 // SMTP username
            $mail->Password = 'Etika@2017';                           // SMTP password

            
                //Recipients
            $mail->setFrom('biofilms@rentals.com', 'BioFilms');
            $mail->addAddress($param['email'], $param['name']);     // Add a recipient
            //   $mail->addAddress('ellen@example.com');               // Name is optional
            $mail->addReplyTo('biofilms@rentals.com', 'BioFilms');
            //   $mail->addCC('cc@example.com');
            //  $mail->addBCC('bcc@example.com');

                //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();

            echo ("<script LANGUAGE='JavaScript'> alert('Thank you for contacting us!!! An email has been forwarded to you. Please check your mail.');</script>");

            echo '<h3> Reading the csv file: </h3>';
            $csv = array_map('str_getcsv', file('contact.csv'));
            echo $csv[sizeof($csv) - 1][0] . '<br>';
            echo $csv[sizeof($csv) - 1][1] . '<br>';
            echo $csv[sizeof($csv) - 1][2] . '<br>';
            echo $csv[sizeof($csv) - 1][3] . '<br>';
            echo $csv[sizeof($csv) - 1][4] . '<br>';

        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ' . $e;

        }

    }
}

?>
