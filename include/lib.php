<?php

class RegisterLogin
{

    /*
     * Register New User
     *
     * @param $name, $email, $phone, $password
     * @return ID
     * */
    public function Register($name, $email, $phone, $password)
    {
        try {
            $db = DB();
            $query = $db->prepare("INSERT INTO register(name, email, phone, password) VALUES (:name,:email,:phone,:password)");
            $query->bindParam("name", $name, PDO::PARAM_STR);
            $query->bindParam("email", $email, PDO::PARAM_STR);
            $query->bindParam("phone", $phone, PDO::PARAM_STR);
            $hash_password = hash('sha256', $password);
            $query->bindParam("password", $hash_password, PDO::PARAM_STR);
            $query->execute();
            return $db->lastInsertId();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function address($user_id, $addr)
    {
        try {
            $db = DB();
            $query = $db->prepare("INSERT INTO profile(user_id, address) VALUES (:user_id,:addr)");
            $query->bindParam("user_id", $user_id, PDO::PARAM_STR);
            $query->bindParam("addr", $addr, PDO::PARAM_STR);
            $query->execute();
            return $db->lastInsertId();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check Phone
     *
     * @param $phone
     * @return boolean
     * */
    public function isPhone($phone)
    {
        try {
            $db = DB();
            $query = $db->prepare("SELECT id FROM register WHERE phone=:phone");
            $query->bindParam("phone", $phone, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check Email
     *
     * @param $email
     * @return boolean
     * */
    public function isEmail($email)
    {
        try {
            $db = DB();
            $query = $db->prepare("SELECT id FROM register WHERE email=:email");
            $query->bindParam("email", $email, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Login
     *
     * @param $email, $password
     * @return $mixed
     * */
    public function Login($email, $password)
    {
        try {
            $db = DB();
            $query = $db->prepare("SELECT id FROM register WHERE email=:email AND password=:password");
            $query->bindParam("email", $email, PDO::PARAM_STR);
            $hash_password = hash('sha256', $password);
            $query->bindParam("password", $hash_password, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->id;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * get User Details
     *
     * @param $id
     * @return $mixed
     * */
    public function UserDetails($id)
    {
        try {
            $db = DB();
            $query = $db->prepare("SELECT name, email, phone,address FROM register AS r INNER JOIN profile AS p on r.id=p.user_id WHERE r.id=:id");
            $query->bindParam("id", $id, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetch(PDO::FETCH_OBJ);
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function updateProfile($id,$name,$email,$phone){
        try{
            $db = DB();
            $query=$db->prepare("UPDATE register set name=:name, email=:email, phone=:phone WHERE id=:id");
            $query->bindParam("id", $id, PDO::PARAM_STR);
            $query->bindParam("name", $name, PDO::PARAM_STR);
            $query->bindParam("email", $email, PDO::PARAM_STR);
            $query->bindParam("phone", $phone, PDO::PARAM_STR);
            $query->execute();
            if($query) {
               return true;       
            }else{
                return false;
            }

        }
        catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function updateAddress($user_id,$address){
        try{
            $db = DB();
            $query=$db->prepare("UPDATE profile set address=:address WHERE user_id=:user_id");
            $query->bindParam("user_id", $user_id, PDO::PARAM_STR);
            $query->bindParam("address", $address, PDO::PARAM_STR);
            $query->execute();
            if($query) {
               return true;       
            }else{
                return false;
            }

        }
        catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
}